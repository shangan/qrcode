package com.arthas.qrcode.main;

import java.io.File;

import com.arthas.qrcode.file.FileSplitCombinUtil;
import com.arthas.qrcode.parse.QRCodeParseHandle;

/**
 * 外网函数
 * 主要是解析二维码和合并文件
 * @author admin
 *
 */
public class OutsideMain {

	public static void main(String[] args) throws Exception {
		// 1.parse qrcode to file
		String inputDir = "C:\\github\\qrcode\\test\\qccode";
		String outputDir= "C:\\github\\qrcode\\test\\result";
//		File[] fileList = new File(inputDir).listFiles();
//		
//		for(File f : fileList) {
//			QRCodeParseHandle.decodeQRCode(f.getPath(), outputDir);
//		}
		
		// 2.combin file
		int partFileLength = 2044;// 指定分割的子文件大小为200字节
		String splitFilePath = outputDir;
		int fileNum = 78;
		String fileName = "test.doc";
		String directoryPath = outputDir;
		FileSplitCombinUtil.combineFile(splitFilePath, directoryPath, fileName, fileNum, partFileLength);
	}
	
}
